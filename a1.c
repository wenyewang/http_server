
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <stdlib.h>

#pragma pack(1)

int portnumber;

// variable of clients
int client[FD_SETSIZE];
int clientNum = 0;
char recvbuf[1024];
char sec_recvbuf[1024];
char sendbuf[2048];
char requestFile[256];
char browserName[256];
char userAgent[256];
fd_set rset, allset;

// values for select
int i, maxi, maxfd, sockfd, sin_size, nready, n, fd_exist;

int agentCnt = 0, agentPos = 0;
char agent[10][160];	///< agent list

/// about page
char about[] = "HTTP/1.1 200 OK\nContent-Type: text/html; charset=utf-8\n\n<html><title>about</title><body><h1>hello world</h1></body></html>";
char httpHead[] = "HTTP/1.1 200 OK\nContent-Type: text/html; charset=utf-8\n\n";

void process(int clientIdx)
{
	sscanf(sec_recvbuf, "GET %s HTTP", requestFile);
	int i = 0, scRet, k, re, j;
	do {
		scRet = sscanf(sec_recvbuf + i, "%s", userAgent);

		for(j = i + 12, k = 0; sec_recvbuf[j] != '\n'; j++, k++) {
			browserName[k] = sec_recvbuf[j];
		}
		for(; sec_recvbuf[i] != '\n'; i++);
		i++;
	} while(strcmp(userAgent, "User-Agent:") != 0);
	
//	printf(" ==== request files : %s ==== \n", requestFile);
//	printf(" ==== browser info : %s ==== \n", browserName);
	
	strcpy(agent[agentPos], browserName);
	agentPos = (agentPos + 1) % 10;
	agentCnt++;
	agentCnt = agentCnt >= 10 ? 10 : agentCnt;
	
	if(strcmp(requestFile, "/") == 0) {
		re = send(client[clientIdx], httpHead, sizeof(httpHead), 0);
//		printf("re : %d \n", re);
		sprintf(sendbuf, "<html><title>brower history</title><body>%d </br>", agentCnt);
		for(k = 0; k < agentCnt; k++) {
			strcat(sendbuf, agent[k]);
			strcat(sendbuf, "</br>");
		}
		strcat(sendbuf, "</body></html>");
		re = send(client[clientIdx], sendbuf, strlen(sendbuf), 0);
//		printf("re : %d \n", re);
	}
	else if(strcmp(requestFile, "/about.html") == 0){
		re = send(client[clientIdx], about, sizeof(about), 0);
//		printf("re : %d \n", re);
	}
	close(client[clientIdx]);
}

int main(int argc, char *argv[])
{
	int listenfd, connfd;
	struct sockaddr_in chiaddr, servaddr;

	sin_size = sizeof(struct sockaddr_in);

    // get port number
    if (argc != 2) {
        fprintf(stderr, "Usage:%s portnumber\n\a", argv[0]);
        exit(1);
    }
    if ((portnumber = atoi(argv[1])) < 0) {
        fprintf(stderr, "invalid portnumber\n\a");
        exit(1);
    }

	// establish socket
	listenfd = socket(AF_INET,SOCK_STREAM,0);
	if(listenfd == -1) {
		printf("Server Error, socket established error %d\n", (char*)strerror(errno));
		exit(1);
	}

	// set socket param
	int opt = SO_REUSEADDR;
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(portnumber);

	// bind socket to ip:port
	if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(struct sockaddr)) == -1) {
		fprintf(stderr, "Server Error, bind error:%s\n\a", strerror(errno));
		exit(1);
	}

	// get server address & show it.
	struct sockaddr_in localaddr;
	int serv_len = sizeof(struct sockaddr_in);
	int re = getsockname(sockfd, (struct sockaddr *)&localaddr, (socklen_t*)&serv_len);
	if(re == 0) {
		char serv_ip[20];
		inet_ntop(AF_INET, &localaddr.sin_addr, serv_ip, sizeof(serv_ip));
		printf("Server Started successfully, local addr : %s\n", serv_ip);
	}

	if(listen(listenfd,SOMAXCONN) == -1) {
		printf("listen error \n");
		exit(1);
	}

	// initialize select
	maxfd = listenfd;
	maxi = -1;
	for(i = 0; i < FD_SETSIZE; i++) {
		client[i] = -1;
	}
	FD_ZERO(&allset);
	FD_SET(listenfd, &allset);

	while(1) {
		struct sockaddr_in addr;
		rset = allset;
		nready = select(maxfd + 1, &rset, NULL, NULL, NULL);

		if(FD_ISSET(listenfd, &rset)) {
			// get a connection from new client.
			if((connfd = accept(listenfd, (struct sockaddr *)&addr, (socklen_t *) & sin_size)) == -1) {
				printf("accept error %s\n", strerror(errno));
				continue;
			}

			for(i = 0; i < FD_SETSIZE; i++) {
				if(client[i] < 0) {
				// add socket connection to client without init it.
					client[i] = connfd;
					if(i == FD_SETSIZE)
						printf("too many clients\n");
					FD_SET(connfd, &allset);
					if(connfd > maxfd)
						maxfd = connfd;
					nready--;
					if(nready <= 0)
						break;
				}
			}
		}

		for(i = 0; i < FD_SETSIZE; i++) {
			if((sockfd = client[i]) < 0)
				continue;

			if(FD_ISSET(sockfd, &rset)) {
				printf(" ==== recv from sockfd %d, recved len = ", sockfd);
				if((n = recv(sockfd, recvbuf, 512, 0)) == 0) {
					//connection closed from client, set the status to inactive
					client[i] = -1;
					printf("%d & ==== \n", n);
					FD_CLR(sockfd, &allset);

					close(sockfd);
				}
				else {
					// process msg;
					// do security check first.
					printf("%d  ==== \n", n);
					char* sec_str = sec_recvbuf;
					int s;
					for(s = 0; s < n; s++) {
						if(recvbuf[s] == '<') {
							*sec_str++ = '&';
							*sec_str++ = 'l';
							*sec_str++ = 't';
							*sec_str++ = ';';
						}
						else if(recvbuf[s] == '>') {
							*sec_str++ = '&';
							*sec_str++ = 'g';
							*sec_str++ = 't';
							*sec_str++ = ';';
						}
						else if(recvbuf[s] == '"') {
							*sec_str++ = '&';
							*sec_str++ = 'q';
							*sec_str++ = 'u';
							*sec_str++ = 'o';
							*sec_str++ = 't';
							*sec_str++ = ';';
						}
						else {
							*sec_str = recvbuf[s];
							sec_str++;
						}
					}
					*sec_str = 0;
					
					for(sec_str = sec_recvbuf; *sec_str != 0; sec_str++)
						printf("%c", *sec_str);
					printf("\n");
					process(i);
				}

				if(--nready <= 0)
					break;
			}
		}
	}


	return 0;
}
